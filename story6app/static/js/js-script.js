document.querySelectorAll('.accordion_button').forEach(button => {
			button.addEventListener('click', () => {
				const accordionContent = button.nextElementSibling;
				
				button.classList.toggle('accordion_button--active');
				
				if (button.classList.contains('accordion_button--active')) {
					accordionContent.style.maxHeight = accordionContent.scrollHeight + 'px';
				}else {
					accordionContent.style.maxHeight = 0;
				}
			});
		});
		
var checkbox = document.querySelector('input[name=theme]');


checkbox.addEventListener('change', function() {
    if(this.checked){
        trans()
        document.documentElement.setAttribute('data-theme', 'dark')
    }else{
        trans()
        document.documentElement.setAttribute('data-theme', 'light')
    }
})
let trans = () => {
    document.documentElement.classList.add('transition');
    window.setTimeout(() => {
        document.documentElement.classList.remove('transition')
    },1000)
}
