# Generated by Django 2.2.6 on 2019-11-03 12:43

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story6app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postmodel',
            name='message',
            field=models.TextField(validators=[django.core.validators.MaxLengthValidator(300)]),
        ),
    ]
